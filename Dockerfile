FROM 136531923082.dkr.ecr.us-west-2.amazonaws.com/alpine-java8:master

LABEL description="dbProvisioning"
LABEL version="0.1"
LABEL vendor="Oyster"

WORKDIR /dbprovision
COPY . /dbprovision

RUN apk add --no-cache --no-progress mysql-client

CMD tail -f /dev/null
